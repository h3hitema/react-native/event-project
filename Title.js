import React, { Component } from 'react';
import { StyleSheet, Text } from 'react-native';


class Title extends Component {

    render() {

        let { text } = this.props;

        console.log('render')
        return (
            <Text style={styles.title} >{text}</Text>
        )
    }

}

export default Title;

const styles = StyleSheet.create({
    title: {
        fontSize: 25,
        color: 'red'
    },
});