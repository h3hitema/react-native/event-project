import React, {Component} from 'react';
import { StyleSheet, View, Text } from 'react-native';
import Title from '../components/Title'
import EventBox from '../components/EventBox';
class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    
    return (
      <View style={styles.container}>
          <Title title={"Ce week-end"}/>
          <EventBox/>
          <Title title={"A venir"}/>
      </View>
    )
  }
}

export default Home;

const styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor: '#fff',
      paddingTop: 70
    },
  });