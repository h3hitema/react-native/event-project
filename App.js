import React, {Component} from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import Splash from './screens/Splash';
import Home from './screens/Home';

const AppNavigator = createStackNavigator(
  {
    Splash: {screen: Splash, navigationOptions: {headerShown: false}},
    Home: {screen: Home, navigationOptions: {headerShown:false}}
  },
  {
    initialRouteName: 'Splash'
  }
)

class App extends Component {

  render() {    
    const AppContainer = createAppContainer(AppNavigator);

    return(<AppContainer/>)
  }

}

export default App;
