import React, { Component } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import city from "../assets/city.jpg";

class EventBox extends Component {

    render() {
        return (
            <View style={styles.container}>
                <Image source={city} style={styles.headerImage} resizeMode={"cover"}/>
                <View style={styles.body}>
                    <Text style={styles.title}>Mon super event</Text>
                    <Text style={styles.subtitle}>Animation</Text>
                    <Text style={styles.subtitle}>12h - 13h</Text>
                </View>
                <Text>Mon event</Text>
            </View>
        )
    }

}

export default EventBox;

const styles = StyleSheet.create({
    container: {
        overflow: "scroll",
        marginHorizontal: 10,
        marginVertical: 20,
        shadowColor:"#000",
        shadowOffset: {
            width:0,
            height: 10
        },
        shadowOpacity: 0.08,
        shadowRadius: 20,
        elevation: 5
    },

    headerImage: {
        height:120,
        width: "100%",
    },

    body: {
        display: "flex",
        justifyContent: "center",
        height: 150,
        backgroundColor: "#FFF",
        borderBottomStartRadius:20,
        borderBottomEndRadius: 20,
        padding: 20
    },

    title: {
        fontSize: 18,
        fontWeight: "600",
    },

    subtitle: {
        fontSize: 16,
        color: "#B0B0B0"
    }
});